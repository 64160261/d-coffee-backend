import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { DwproductsService } from './dwproducts.service';
import { CreateDwproductDto } from './dto/create-dwproduct.dto';
import { UpdateDwproductDto } from './dto/update-dwproduct.dto';

@Controller('dwproducts')
export class DwproductsController {
  constructor(private readonly dwproductsService: DwproductsService) {}

  @Post()
  create(@Body() createDwproductDto: CreateDwproductDto) {
    return this.dwproductsService.create(createDwproductDto);
  }

  @Get()
  findAll() {
    return this.dwproductsService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.dwproductsService.findOne(+id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateDwproductDto: UpdateDwproductDto,
  ) {
    return this.dwproductsService.update(+id, updateDwproductDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.dwproductsService.remove(+id);
  }
}
