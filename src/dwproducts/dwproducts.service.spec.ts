import { Test, TestingModule } from '@nestjs/testing';
import { DwproductsService } from './dwproducts.service';

describe('DwproductsService', () => {
  let service: DwproductsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [DwproductsService],
    }).compile();

    service = module.get<DwproductsService>(DwproductsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
