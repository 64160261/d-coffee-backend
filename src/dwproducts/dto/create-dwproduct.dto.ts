import { IsNotEmpty } from 'class-validator';

export class CreateDwproductDto {
  @IsNotEmpty()
  Product_Name: string;

  @IsNotEmpty()
  Product_Size: string;

  @IsNotEmpty()
  Product_Price: number;
}
