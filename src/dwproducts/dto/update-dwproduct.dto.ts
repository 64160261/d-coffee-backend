import { PartialType } from '@nestjs/mapped-types';
import { CreateDwproductDto } from './create-dwproduct.dto';

export class UpdateDwproductDto extends PartialType(CreateDwproductDto) {}
