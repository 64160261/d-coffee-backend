import { Test, TestingModule } from '@nestjs/testing';
import { DwproductsController } from './dwproducts.controller';
import { DwproductsService } from './dwproducts.service';

describe('DwproductsController', () => {
  let controller: DwproductsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [DwproductsController],
      providers: [DwproductsService],
    }).compile();

    controller = module.get<DwproductsController>(DwproductsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
