import { Injectable } from '@nestjs/common';
import { CreateDwproductDto } from './dto/create-dwproduct.dto';
import { UpdateDwproductDto } from './dto/update-dwproduct.dto';

@Injectable()
export class DwproductsService {
  create(createDwproductDto: CreateDwproductDto) {
    return 'This action adds a new dwproduct';
  }

  findAll() {
    return `This action returns all dwproducts`;
  }

  findOne(id: number) {
    return `This action returns a #${id} dwproduct`;
  }

  update(id: number, updateDwproductDto: UpdateDwproductDto) {
    return `This action updates a #${id} dwproduct`;
  }

  remove(id: number) {
    return `This action removes a #${id} dwproduct`;
  }
}
