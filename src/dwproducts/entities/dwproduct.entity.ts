import { Order } from 'src/orders/entities/order.entity';
import { Reciept } from 'src/reciepts/entities/reciept.entity';
import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Dwproducts {
  @PrimaryGeneratedColumn()
  Product_Id: number;

  @Column()
  Product_Name: string;

  @Column({
    type: 'float',
  })
  Product_Price: number;

  @Column({
    length: '32',
  })
  Product_Size: string;

  @Column()
  Product_Catagory: number;
}
