import { Module } from '@nestjs/common';
import { DwproductsService } from './dwproducts.service';
import { DwproductsController } from './dwproducts.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Dwproducts } from './entities/dwproduct.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Dwproducts])],
  controllers: [DwproductsController],
  providers: [DwproductsService],
})
export class DwproductsModule {}
