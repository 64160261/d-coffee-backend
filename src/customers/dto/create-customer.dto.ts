import { IsNotEmpty, Length } from 'class-validator';

export class CreateCustomerDto {
  @IsNotEmpty()
  name: string;

  @IsNotEmpty()
  @Length(10)
  tel: string;

  @IsNotEmpty()
  point: number;
}
