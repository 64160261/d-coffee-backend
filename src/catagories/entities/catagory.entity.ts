import { Product } from 'src/products/entities/product.entity';
import { PrimaryGeneratedColumn, Column, Entity, OneToMany } from 'typeorm';

@Entity()
export class Catagory {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    length: '32',
  })
  name: string;

  @OneToMany(() => Product, (product) => product.catagory)
  products: Product[];
}
