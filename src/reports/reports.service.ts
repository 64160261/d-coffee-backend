import { Injectable } from '@nestjs/common';
import { CreateReportDto } from './dto/create-report.dto';
import { UpdateReportDto } from './dto/update-report.dto';
import { InjectDataSource } from '@nestjs/typeorm';
import { DataSource } from 'typeorm';

@Injectable()
export class ReportsService {
  constructor(@InjectDataSource() private dataSource: DataSource) {}

  // stored
  getProduct1() {
    return this.dataSource.query('CALL getProduct()');
  }
  // stored
  getUser() {
    return this.dataSource.query('CALL getUser()');
  }

  getdelOrder() {
    return this.dataSource.query('CALL delOrder()');
  }
  // view
  getCustomerInfo() {
    return this.dataSource.query('SELECT * FROM getCustomer_info');
  }

  getlowMaterial() {
    return this.dataSource.query('SELECT * FROM lowMaterial');
  }

  getCustomerByPoint() {
    return this.dataSource.query('SELECT * FROM getCustomerByPoint');
  }

  getProductBySearchText(searchText: any) {
    return this.dataSource.query('SELECT * FROM product WHERE name LIKE ?', [
      `%${searchText}%`,
    ]);
  }

  getCustomerBySearchText(searchText: any) {
    return this.dataSource.query('SELECT * FROM customer WHERE name LIKE ?', [
      `%${searchText}%`,
    ]);
  }

  getCustomerTelBySearchText(searchText: any) {
    return this.dataSource.query('SELECT * FROM customer WHERE tel LIKE ?', [
      `%${searchText}%`,
    ]);
  }

  getUserBySearchText(searchText: any) {
    return this.dataSource.query('SELECT * FROM user WHERE name LIKE ?', [
      `%${searchText}%`,
    ]);
  }

  create(createReportDto: CreateReportDto) {
    return 'This action adds a new report';
  }

  findAll() {
    return `This action returns all reports`;
  }

  findOne(id: number) {
    return `This action returns a #${id} report`;
  }

  update(id: number, updateReportDto: UpdateReportDto) {
    return `This action updates a #${id} report`;
  }

  remove(id: number) {
    return `This action removes a #${id} report`;
  }
}
