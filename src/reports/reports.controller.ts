import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Query,
} from '@nestjs/common';
import { ReportsService } from './reports.service';
import { CreateReportDto } from './dto/create-report.dto';
import { UpdateReportDto } from './dto/update-report.dto';
import { query } from 'express';
import { get } from 'http';

@Controller('reports')
export class ReportsController {
  constructor(private readonly reportsService: ReportsService) {}

  /*   @Get('/product')
  getProduct(
    @Query()
    query: {
      lowPrice?: number;
      upperPrice?: number;
      searchText?: string;
    },
  ) {
    if (query.searchText) {
      return this.reportsService.getProductBySearchText(query.searchText);
    }
    return this.reportsService.getProduct();
  } */

  @Get('/customerbypoint')
  getCustomerByPoint() {
    return this.reportsService.getCustomerByPoint();
  }

  @Get('/lowMaterial')
  getlowMaterial() {
    return this.reportsService.getlowMaterial();
  }

  @Get('/product1')
  getProduct1() {
    return this.reportsService.getProduct1();
  }

  @Delete('/delOrder')
  getdelOrder() {
    return this.reportsService.getdelOrder();
  }

  @Get('/customer')
  getCustomer(
    @Query()
    query: {
      searchText?: string;
    },
  ) {
    if (query.searchText) {
      return this.reportsService.getCustomerTelBySearchText(query.searchText);
    }
    return this.reportsService.getCustomerInfo();
  }

  @Get('/user')
  getUser(
    @Query()
    query: {
      searchText?: string;
    },
  ) {
    if (query.searchText) {
      return this.reportsService.getUserBySearchText(query.searchText);
    }
    return this.reportsService.getUser();
  }

  // @Post()
  // create(@Body() createReportDto: CreateReportDto) {
  //   return this.reportsService.create(createReportDto);
  // }

  // @Get()
  // findAll() {
  //   return this.reportsService.findAll();
  // }

  // @Get(':id')
  // findOne(@Param('id') id: string) {
  //   return this.reportsService.findOne(+id);
  // }

  // @Patch(':id')
  // update(@Param('id') id: string, @Body() updateReportDto: UpdateReportDto) {
  //   return this.reportsService.update(+id, updateReportDto);
  // }

  // @Delete(':id')`
  // remove(@Param('id') id: string) {
  //   return this.reportsService.remove(+id);
  // }
}
