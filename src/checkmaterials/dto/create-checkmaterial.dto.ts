import { IsNotEmpty } from 'class-validator';

export class CreateCheckmaterialDto {
  @IsNotEmpty()
  date: string;

  @IsNotEmpty()
  time: string;
}
