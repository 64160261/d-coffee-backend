import { PartialType } from '@nestjs/mapped-types';
import { CreateCheckmaterialDto } from './create-checkmaterial.dto';

export class UpdateCheckmaterialDto extends PartialType(
  CreateCheckmaterialDto,
) {}
