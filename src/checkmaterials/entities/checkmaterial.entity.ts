import { Employee } from 'src/employees/entities/employee.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToOne,
  OneToMany,
} from 'typeorm';
import { CheckmaterialDetail } from './checkmaterial-detail';

@Entity()
export class Checkmaterial {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  date: string;

  @Column()
  time: string;

  @ManyToOne(() => Employee, (employee) => employee.checkmaterials)
  employee: Employee;

  @OneToMany(
    () => CheckmaterialDetail,
    (checkmaterialDetail) => checkmaterialDetail.checkmaterial,
  )
  checkmaterialDetails: CheckmaterialDetail[];
}
