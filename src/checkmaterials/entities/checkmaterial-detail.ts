import { Employee } from 'src/employees/entities/employee.entity';
import { Material } from 'src/materials/entities/material.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToOne,
  OneToMany,
  CreateDateColumn,
  UpdateDateColumn,
  DeleteDateColumn,
} from 'typeorm';
import { Checkmaterial } from './checkmaterial.entity';

@Entity()
export class CheckmaterialDetail {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  qtyLast: string;

  @Column()
  qtyRemain: string;

  @Column()
  qtyExpire: string;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @DeleteDateColumn()
  deletedAt: Date;

  @ManyToOne(
    () => Checkmaterial,
    (checkmaterial) => checkmaterial.checkmaterialDetails,
  )
  checkmaterial: Checkmaterial;

  @ManyToOne(() => Material, (material) => material.checkmaterialDetails)
  material: Material;
}
