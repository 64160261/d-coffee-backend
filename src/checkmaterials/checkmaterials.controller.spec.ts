import { Test, TestingModule } from '@nestjs/testing';
import { CheckmaterialsController } from './checkmaterials.controller';
import { CheckmaterialsService } from './checkmaterials.service';

describe('CheckmaterialsController', () => {
  let controller: CheckmaterialsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [CheckmaterialsController],
      providers: [CheckmaterialsService],
    }).compile();

    controller = module.get<CheckmaterialsController>(CheckmaterialsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
