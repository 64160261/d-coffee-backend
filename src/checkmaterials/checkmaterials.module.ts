import { Module } from '@nestjs/common';
import { CheckmaterialsService } from './checkmaterials.service';
import { CheckmaterialsController } from './checkmaterials.controller';
import { Checkmaterial } from './entities/checkmaterial.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CheckmaterialDetail } from './entities/checkmaterial-detail';

@Module({
  imports: [TypeOrmModule.forFeature([Checkmaterial, CheckmaterialDetail])],
  controllers: [CheckmaterialsController],
  providers: [CheckmaterialsService],
})
export class CheckmaterialsModule {}
