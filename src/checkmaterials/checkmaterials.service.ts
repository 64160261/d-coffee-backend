import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateCheckmaterialDto } from './dto/create-checkmaterial.dto';
import { UpdateCheckmaterialDto } from './dto/update-checkmaterial.dto';
import { Checkmaterial } from './entities/checkmaterial.entity';

@Injectable()
export class CheckmaterialsService {
  constructor(
    @InjectRepository(Checkmaterial)
    private checkmaterialsRepository: Repository<Checkmaterial>,
  ) {}

  create(createCheckmaterialDto: CreateCheckmaterialDto) {
    return this.checkmaterialsRepository.save(createCheckmaterialDto);
  }

  findAll() {
    return this.checkmaterialsRepository.find({});
  }

  findOne(id: number) {
    return this.checkmaterialsRepository.findOne({ where: { id: id } });
  }

  async update(id: number, updateCheckmaterialDto: UpdateCheckmaterialDto) {
    try {
      const updatedCheckmaterial = await this.checkmaterialsRepository.save({
        id,
        ...updateCheckmaterialDto,
      });
      return updatedCheckmaterial;
    } catch (e) {
      throw new NotFoundException();
    }
  }

  async remove(id: number) {
    const checkmaterial = await this.checkmaterialsRepository.findOne({
      where: { id: id },
    });
    try {
      const deletedCheckmaterial = await this.checkmaterialsRepository.remove(
        checkmaterial,
      );
      return deletedCheckmaterial;
    } catch (e) {
      throw new NotFoundException();
    }
  }
}
