import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateEmployeeDto } from './dto/create-employee.dto';
import { UpdateEmployeeDto } from './dto/update-employee.dto';
import { Employee } from './entities/employee.entity';

@Injectable()
export class EmployeesService {
  constructor(
    @InjectRepository(Employee)
    private employeesRepository: Repository<Employee>,
  ) {}

  create(createEmployeeDto: CreateEmployeeDto) {
    return this.employeesRepository.save(createEmployeeDto);
  }

  findAll() {
    return this.employeesRepository.find({});
  }

  async findOne(id: number) {
    const Employee = await this.employeesRepository.findOne({
      where: { id: id },
    });
    if (!Employee) {
      throw new NotFoundException();
    }
    return Employee;
  }

  async update(id: number, updateEmployeeDto: UpdateEmployeeDto) {
    const Employee = await this.employeesRepository.findOneBy({ id: id });
    if (!Employee) {
      throw new NotFoundException();
    }
    const updatedEmployee = { ...Employee, ...updateEmployeeDto };
    return this.employeesRepository.save(updatedEmployee);
  }

  async remove(id: number) {
    const Employee = await this.employeesRepository.findOneBy({ id: id });
    if (!Employee) {
      throw new NotFoundException();
    }
    return this.employeesRepository.softRemove(Employee);
  }
}
