import { IsNotEmpty, Length } from 'class-validator';

export class CreateEmployeeDto {
  @IsNotEmpty()
  name: string;

  @IsNotEmpty()
  @Length(10)
  tel: string;

  @IsNotEmpty()
  email: string;

  @IsNotEmpty()
  position: string;

  @IsNotEmpty()
  hourly_wage: number;
}
