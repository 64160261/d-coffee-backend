import { Bill } from 'src/bills/entities/bill.entity';
import { Checkinout } from 'src/checkinouts/entities/checkinout.entity';
import { Checkmaterial } from 'src/checkmaterials/entities/checkmaterial.entity';
import { Order } from 'src/orders/entities/order.entity';
import { Reciept } from 'src/reciepts/entities/reciept.entity';
import { User } from 'src/users/entities/user.entity';
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  JoinColumn,
  OneToMany,
  OneToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
export class Employee {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  tel: string;

  @Column()
  email: string;

  @Column()
  position: string;

  @Column()
  hourly_wage: number;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @DeleteDateColumn()
  deletedAt: Date;

  @OneToMany(() => Reciept, (reciept) => reciept.employee)
  reciepts: Reciept[];

  @OneToMany(() => Order, (order) => order.employee)
  orders: Order[];

  @OneToMany(() => Checkmaterial, (checkmaterial) => checkmaterial.employee)
  checkmaterials: Checkmaterial[];

  @OneToMany(() => Bill, (bill) => bill.employee)
  bills: Bill[];

  @OneToMany(() => Checkinout, (checkinout) => checkinout.employee)
  checkinouts: Checkinout[];

  @OneToOne(() => User, (users) => users.employee)
  @JoinColumn()
  user: User;
}
