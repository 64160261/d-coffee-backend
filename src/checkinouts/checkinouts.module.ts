import { Module } from '@nestjs/common';
import { CheckinoutsService } from './checkinouts.service';
import { CheckinoutsController } from './checkinouts.controller';
import { Checkinout } from './entities/checkinout.entity';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [TypeOrmModule.forFeature([Checkinout])],
  controllers: [CheckinoutsController],
  providers: [CheckinoutsService],
})
export class CheckinoutsModule {}
