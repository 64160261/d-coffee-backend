import { Bill } from 'src/bills/entities/bill.entity';
import { Checkmaterial } from 'src/checkmaterials/entities/checkmaterial.entity';
import { Employee } from 'src/employees/entities/employee.entity';
import { Reciept } from 'src/reciepts/entities/reciept.entity';
import { Summarysalary } from 'src/summarysalarys/entities/summarysalary.entity';
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
export class Checkinout {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  date: string;

  @Column()
  time_in: string;

  @Column()
  time_out: string;

  @Column()
  total_hour: string;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @DeleteDateColumn()
  deletedAt: Date;

  @ManyToOne(() => Employee, (employee) => employee.checkinouts)
  employee: Employee;

  @ManyToOne(() => Summarysalary, (summarysalary) => summarysalary.checkinouts)
  summarysalary: Summarysalary;
}
