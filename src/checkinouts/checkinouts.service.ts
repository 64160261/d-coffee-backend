import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateCheckinoutDto } from './dto/create-checkinout.dto';
import { UpdateCheckinoutDto } from './dto/update-checkinout.dto';
import { Checkinout } from './entities/checkinout.entity';

@Injectable()
export class CheckinoutsService {
  constructor(
    @InjectRepository(Checkinout)
    private checkinoutsRepository: Repository<Checkinout>,
  ) {}

  create(createCheckinoutDto: CreateCheckinoutDto) {
    return this.checkinoutsRepository.save(createCheckinoutDto);
  }

  findAll() {
    return this.checkinoutsRepository.find({});
  }

  async findOne(id: number) {
    const Checkinout = await this.checkinoutsRepository.findOne({
      where: { id: id },
    });
    if (!Checkinout) {
      throw new NotFoundException();
    }
    return Checkinout;
  }

  async update(id: number, updateCheckinoutDto: UpdateCheckinoutDto) {
    const Checkinout = await this.checkinoutsRepository.findOneBy({ id: id });
    if (!Checkinout) {
      throw new NotFoundException();
    }
    const updatedCheckinout = { ...Checkinout, ...updateCheckinoutDto };
    return this.checkinoutsRepository.save(updatedCheckinout);
  }

  async remove(id: number) {
    const Checkinout = await this.checkinoutsRepository.findOneBy({ id: id });
    if (!Checkinout) {
      throw new NotFoundException();
    }
    return this.checkinoutsRepository.softRemove(Checkinout);
  }
}
