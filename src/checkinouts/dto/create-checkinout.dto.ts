import { IsNotEmpty } from 'class-validator';

export class CreateCheckinoutDto {
  @IsNotEmpty()
  date: string;

  @IsNotEmpty()
  time_in: string;

  time_out: string;

  total_hour: string;
}
