import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DataSource } from 'typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { CustomersModule } from './customers/customers.module';
import { Customer } from './customers/entities/customer.entity';
import { EmployeesModule } from './employees/employees.module';
import { Employee } from './employees/entities/employee.entity';
import { ProductsModule } from './products/products.module';
import { CatagoriesModule } from './catagories/catagories.module';
import { Product } from './products/entities/product.entity';
import { Catagory } from './catagories/entities/catagory.entity';
import { StoresModule } from './stores/stores.module';
import { Store } from './stores/entities/store.entity';
import { SummarysalarysModule } from './summarysalarys/summarysalarys.module';
import { UsersModule } from './users/users.module';
import { User } from './users/entities/user.entity';
import { MaterialsModule } from './materials/materials.module';
import { Material } from './materials/entities/material.entity';
import { BillsModule } from './bills/bills.module';
import { Bill } from './bills/entities/bill.entity';
import { Summarysalary } from './summarysalarys/entities/summarysalary.entity';
import { CheckmaterialsModule } from './checkmaterials/checkmaterials.module';
import { Checkmaterial } from './checkmaterials/entities/checkmaterial.entity';
import { RecieptsModule } from './reciepts/reciepts.module';
import { Reciept } from './reciepts/entities/reciept.entity';
import { AuthModule } from './auth/auth.module';
import { RecieptDetail } from './reciepts/entities/reciept-detail';
import { CheckmaterialDetail } from './checkmaterials/entities/checkmaterial-detail';
import { BillDetail } from './bills/entities/bill-detail';
import { CheckinoutsModule } from './checkinouts/checkinouts.module';
import { Checkinout } from './checkinouts/entities/checkinout.entity';
import { OrdersModule } from './orders/orders.module';
import { Order } from './orders/entities/order.entity';
import { OrderItem } from './orders/entities/order-item';
import { ReportsModule } from './reports/reports.module';
import { DwproductsModule } from './dwproducts/dwproducts.module';
import { DwstoresModule } from './dwstores/dwstores.module';
import { DwcustomersModule } from './dwcustomers/dwcustomers.module';
import { DwemployeesModule } from './dwemployees/dwemployees.module';
import { DwtimesModule } from './dwtimes/dwtimes.module';
import { DwtimemonthModule } from './dwtimemonth/dwtimemonth.module';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: 'projectdb.clmm6o84glw3.us-east-1.rds.amazonaws.com',
      // host: 'db4free.net',
      port: 3306,
      username: 'admin',
      password: '456789Asd-',
      database: 'DBProject',
      // username: 'test_coffee_db',
      // password: 'Pass@1234',
      // database: 'test_coffee_db',
      entities: [
        Customer,
        Employee,
        Store,
        Product,
        Catagory,
        User,
        Material,
        Bill,
        Summarysalary,
        Checkmaterial,
        Reciept,
        RecieptDetail,
        CheckmaterialDetail,
        BillDetail,
        Checkinout,
        Order,
        OrderItem,
      ],
      synchronize: true,
    }),
    SummarysalarysModule,
    CheckmaterialsModule,
    BillsModule,
    MaterialsModule,
    UsersModule,
    CustomersModule,
    EmployeesModule,
    ProductsModule,
    CatagoriesModule,
    StoresModule,
    RecieptsModule,
    AuthModule,
    CheckinoutsModule,
    OrdersModule,
    ReportsModule,
    DwproductsModule,
    DwstoresModule,
    DwcustomersModule,
    DwemployeesModule,
    DwtimesModule,
    DwtimemonthModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {
  constructor(private dataSource: DataSource) { }
}
