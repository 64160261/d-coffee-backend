import { Order } from 'src/orders/entities/order.entity';
import { Reciept } from 'src/reciepts/entities/reciept.entity';
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
export class Store {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  province: string;

  @Column()
  district: string;

  @Column()
  subdistrict: string;

  @Column()
  zip: string;

  @Column()
  tel: string;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @DeleteDateColumn()
  deletedAt: Date;

  @OneToMany(() => Reciept, (recipet) => recipet.store)
  reciept: Reciept[];

  @OneToMany(() => Order, (order) => order.store)
  orders: Order[];
}
