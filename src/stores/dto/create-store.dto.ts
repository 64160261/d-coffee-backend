import { IsNotEmpty, Length } from 'class-validator';
export class CreateStoreDto {
  @IsNotEmpty()
  name: string;

  @IsNotEmpty()
  province: string;

  @IsNotEmpty()
  district: string;

  @IsNotEmpty()
  subdistrict: string;

  @IsNotEmpty()
  @Length(5)
  zip: string;

  @IsNotEmpty()
  @Length(10)
  tel: string;
}
