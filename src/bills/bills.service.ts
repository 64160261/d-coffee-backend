import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateBillDto } from './dto/create-bill.dto';
import { UpdateBillDto } from './dto/update-bill.dto';
import { Bill } from './entities/bill.entity';

@Injectable()
export class BillsService {
  constructor(
    @InjectRepository(Bill)
    private billsRepository: Repository<Bill>,
  ) {}

  create(createBillDto: CreateBillDto) {
    return this.billsRepository.save(createBillDto);
  }

  findAll() {
    return this.billsRepository.find({});
  }

  async findOne(id: number) {
    const bill = await this.billsRepository.findOne({
      where: { id: id },
    });
    if (!bill) {
      throw new NotFoundException();
    }
    return bill;
  }

  async update(id: number, updateBillDto: UpdateBillDto) {
    const bill = await this.billsRepository.findOneBy({ id: id });
    if (!bill) {
      throw new NotFoundException();
    }
    const updatedBill = { ...bill, ...updateBillDto };
    return this.billsRepository.save(updatedBill);
  }

  async remove(id: number) {
    const bill = await this.billsRepository.findOneBy({ id: id });
    if (!bill) {
      throw new NotFoundException();
    }
    return this.billsRepository.softRemove(bill);
  }
}
