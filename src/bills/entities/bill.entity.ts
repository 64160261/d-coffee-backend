import { Employee } from 'src/employees/entities/employee.entity';
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { BillDetail } from './bill-detail';

@Entity()
export class Bill {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  shopname: string;

  @Column()
  date: Date;

  @Column()
  time: Date;

  @Column()
  total: number;

  @Column()
  buy: number;

  @Column()
  change: string;

  @CreateDateColumn()
  start_date: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @DeleteDateColumn()
  deletedAt: Date;

  @ManyToOne(() => Employee, (employee) => employee.bills)
  employee: Employee;

  @OneToMany(() => BillDetail, (billDetail) => billDetail.bill)
  billDetails: BillDetail;
}
