import { Module } from '@nestjs/common';
import { BillsService } from './bills.service';
import { BillsController } from './bills.controller';
import { Bill } from './entities/bill.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { BillDetail } from './entities/bill-detail';

@Module({
  imports: [TypeOrmModule.forFeature([Bill, BillDetail])],
  controllers: [BillsController],
  providers: [BillsService],
})
export class BillsModule {}
