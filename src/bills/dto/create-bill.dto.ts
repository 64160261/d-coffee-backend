import { IsNotEmpty } from 'class-validator';
export class CreateBillDto {
  @IsNotEmpty()
  shopname: string;

  @IsNotEmpty()
  date: Date;

  @IsNotEmpty()
  time: Date;

  @IsNotEmpty()
  total: number;

  @IsNotEmpty()
  buy: number;

  @IsNotEmpty()
  change: string;
}
