import { IsNotEmpty, IsPositive } from 'class-validator';

class createOrderItemDTO {
  @IsNotEmpty()
  @IsPositive()
  productId: number;

  @IsNotEmpty()
  amount: number;

  discount: number;
}

export class CreateOrderDto {
  customerId: number;

  employeeId: number;

  storeId: number;

  @IsNotEmpty()
  orderItems: createOrderItemDTO[];

  @IsNotEmpty()
  received: number;

  @IsNotEmpty()
  change: number;

  discount: number;

  @IsNotEmpty()
  payment: string;

  tel?: string;
}
