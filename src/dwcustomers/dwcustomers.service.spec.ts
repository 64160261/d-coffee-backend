import { Test, TestingModule } from '@nestjs/testing';
import { DwcustomersService } from './dwcustomers.service';

describe('DwcustomersService', () => {
  let service: DwcustomersService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [DwcustomersService],
    }).compile();

    service = module.get<DwcustomersService>(DwcustomersService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
