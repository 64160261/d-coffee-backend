import { IsNotEmpty, Length } from 'class-validator';
export class CreateDwcustomerDto {
  @IsNotEmpty()
  name: string;

  @IsNotEmpty()
  @Length(10)
  tel: string;

  @IsNotEmpty()
  point: number;
}
