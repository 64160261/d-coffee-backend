import { PartialType } from '@nestjs/mapped-types';
import { CreateDwcustomerDto } from './create-dwcustomer.dto';

export class UpdateDwcustomerDto extends PartialType(CreateDwcustomerDto) {}
