import { Module } from '@nestjs/common';
import { DwcustomersService } from './dwcustomers.service';
import { DwcustomersController } from './dwcustomers.controller';
import { Dwcustomers } from './entities/dwcustomer.entity';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [TypeOrmModule.forFeature([Dwcustomers])],
  controllers: [DwcustomersController],
  providers: [DwcustomersService],
})
export class DwcustomersModule {}
