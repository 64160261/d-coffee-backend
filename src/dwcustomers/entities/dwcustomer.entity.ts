import { Order } from 'src/orders/entities/order.entity';
import { Reciept } from 'src/reciepts/entities/reciept.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity()
export class Dwcustomers {
  @PrimaryGeneratedColumn()
  Customer_Id: number;

  @Column()
  Customer_Name: string;

  @Column()
  Customer_Tel: string;

  @Column()
  Customer_Point: number;

  @CreateDateColumn()
  Customer_start_date: string;
}
