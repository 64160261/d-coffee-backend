import { Injectable } from '@nestjs/common';
import { CreateDwcustomerDto } from './dto/create-dwcustomer.dto';
import { UpdateDwcustomerDto } from './dto/update-dwcustomer.dto';

@Injectable()
export class DwcustomersService {
  create(createDwcustomerDto: CreateDwcustomerDto) {
    return 'This action adds a new dwcustomer';
  }

  findAll() {
    return `This action returns all dwcustomers`;
  }

  findOne(id: number) {
    return `This action returns a #${id} dwcustomer`;
  }

  update(id: number, updateDwcustomerDto: UpdateDwcustomerDto) {
    return `This action updates a #${id} dwcustomer`;
  }

  remove(id: number) {
    return `This action removes a #${id} dwcustomer`;
  }
}
