import { Test, TestingModule } from '@nestjs/testing';
import { DwcustomersController } from './dwcustomers.controller';
import { DwcustomersService } from './dwcustomers.service';

describe('DwcustomersController', () => {
  let controller: DwcustomersController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [DwcustomersController],
      providers: [DwcustomersService],
    }).compile();

    controller = module.get<DwcustomersController>(DwcustomersController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
