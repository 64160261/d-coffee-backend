import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { DwcustomersService } from './dwcustomers.service';
import { CreateDwcustomerDto } from './dto/create-dwcustomer.dto';
import { UpdateDwcustomerDto } from './dto/update-dwcustomer.dto';

@Controller('dwcustomers')
export class DwcustomersController {
  constructor(private readonly dwcustomersService: DwcustomersService) {}

  @Post()
  create(@Body() createDwcustomerDto: CreateDwcustomerDto) {
    return this.dwcustomersService.create(createDwcustomerDto);
  }

  @Get()
  findAll() {
    return this.dwcustomersService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.dwcustomersService.findOne(+id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateDwcustomerDto: UpdateDwcustomerDto,
  ) {
    return this.dwcustomersService.update(+id, updateDwcustomerDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.dwcustomersService.remove(+id);
  }
}
