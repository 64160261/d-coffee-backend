import { IsNotEmpty, IsNumber, IsDate } from 'class-validator';

export class CreateRecieptDto {
  @IsNotEmpty()
  @IsNumber()
  queue: number;

  @IsNotEmpty()
  @IsDate()
  date: Date;

  @IsNotEmpty()
  @IsDate()
  time: Date;

  @IsNotEmpty()
  @IsNumber()
  discount: number;

  @IsNotEmpty()
  @IsNumber()
  total: number;

  @IsNotEmpty()
  @IsNumber()
  received: number;

  @IsNotEmpty()
  @IsNumber()
  change: number;

  @IsNotEmpty()
  payment: string;
}
