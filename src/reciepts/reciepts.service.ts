import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateRecieptDto } from './dto/create-reciept.dto';
import { UpdateRecieptDto } from './dto/update-reciept.dto';
import { Reciept } from './entities/reciept.entity';

@Injectable()
export class RecieptsService {
  constructor(
    @InjectRepository(Reciept)
    private recieptsRepository: Repository<Reciept>,
  ) {}

  create(createRecieptDto: CreateRecieptDto) {
    return this.recieptsRepository.save(createRecieptDto);
  }

  findAll() {
    return this.recieptsRepository.find({});
  }

  async findOne(id: number) {
    const Reciept = await this.recieptsRepository.findOne({
      where: { id: id },
    });
    if (!Reciept) {
      throw new NotFoundException();
    }
    return Reciept;
  }

  async update(id: number, updateRecieptDto: UpdateRecieptDto) {
    const Reciept = await this.recieptsRepository.findOneBy({ id: id });
    if (!Reciept) {
      throw new NotFoundException();
    }
    const updatedReciept = { ...Reciept, ...updateRecieptDto };
    return this.recieptsRepository.save(updatedReciept);
  }

  async remove(id: number) {
    const Reciept = await this.recieptsRepository.findOneBy({ id: id });
    if (!Reciept) {
      throw new NotFoundException();
    }
    return this.recieptsRepository.softRemove(Reciept);
  }
}
