import { Customer } from 'src/customers/entities/customer.entity';
import { Employee } from 'src/employees/entities/employee.entity';
import { Store } from 'src/stores/entities/store.entity';
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { RecieptDetail } from './reciept-detail';

@Entity()
export class Reciept {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  queue: number;

  @Column()
  date: Date;

  @Column()
  time: Date;

  @Column()
  discount: number;

  @Column()
  total: number;

  @Column()
  received: number;

  @Column()
  change: number;

  @Column()
  payment: string;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @DeleteDateColumn()
  deletedAt: Date;

  @ManyToOne(() => Store, (store) => store.reciept)
  store: Store;

  @OneToMany(() => RecieptDetail, (recieptDetails) => recieptDetails.reciept)
  recieptDetails: RecieptDetail[];

  @ManyToOne(() => Customer, (customers) => customers.reciepts)
  customer: Customer;

  @ManyToOne(() => Reciept, (reciept) => reciept.employee)
  employee: Employee;
}
