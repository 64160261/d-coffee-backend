import { Checkinout } from 'src/checkinouts/entities/checkinout.entity';
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
export class Summarysalary {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  ss_date: Date;

  @Column()
  ss_work_hour: number;

  @Column()
  ss_salary: number;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @DeleteDateColumn()
  deletedAt: Date;

  @OneToMany(() => Checkinout, (checkinout) => checkinout.summarysalary)
  checkinouts: Checkinout[];
}
