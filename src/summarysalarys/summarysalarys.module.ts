import { Module } from '@nestjs/common';
import { SummarysalarysService } from './summarysalarys.service';
import { SummarysalarysController } from './summarysalarys.controller';
import { Summarysalary } from './entities/summarysalary.entity';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [TypeOrmModule.forFeature([Summarysalary])],
  controllers: [SummarysalarysController],
  providers: [SummarysalarysService],
})
export class SummarysalarysModule {}
