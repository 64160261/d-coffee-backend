import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateSummarysalaryDto } from './dto/create-summarysalary.dto';
import { UpdateSummarysalaryDto } from './dto/update-summarysalary.dto';
import { Summarysalary } from './entities/summarysalary.entity';

@Injectable()
export class SummarysalarysService {
  constructor(
    @InjectRepository(Summarysalary)
    private summarysalarysRepository: Repository<Summarysalary>,
  ) {}

  create(createSummarysalaryDto: CreateSummarysalaryDto) {
    return this.summarysalarysRepository.save(createSummarysalaryDto);
  }

  findAll() {
    return this.summarysalarysRepository.find({});
  }

  findOne(id: number) {
    return this.summarysalarysRepository.findOne({ where: { id: id } });
  }

  async update(id: number, updateSummarysalaryDto: UpdateSummarysalaryDto) {
    try {
      const updatedSummarysalary = await this.summarysalarysRepository.save({
        id,
        ...updateSummarysalaryDto,
      });
      return updatedSummarysalary;
    } catch (e) {
      throw new NotFoundException();
    }
  }

  async remove(id: number) {
    const summarysalary = await this.summarysalarysRepository.findOne({
      where: { id: id },
    });
    try {
      const deletedSummarysalary = await this.summarysalarysRepository.remove(
        summarysalary,
      );
      return deletedSummarysalary;
    } catch (e) {
      throw new NotFoundException();
    }
  }
}
