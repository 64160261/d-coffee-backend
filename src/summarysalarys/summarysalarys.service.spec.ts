import { Test, TestingModule } from '@nestjs/testing';
import { SummarysalarysService } from './summarysalarys.service';

describe('SummarysalarysService', () => {
  let service: SummarysalarysService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [SummarysalarysService],
    }).compile();

    service = module.get<SummarysalarysService>(SummarysalarysService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
