import { IsNotEmpty, Length, IsNumber, Min } from 'class-validator';

export class CreateProductDto {
  @IsNotEmpty()
  name: string;

  @IsNotEmpty()
  size: string;

  @IsNotEmpty()
  price: number;

  image = 'no_img_avaliable.jpg';

  @IsNotEmpty()
  catagoryId: number;
}
