import { Catagory } from 'src/catagories/entities/catagory.entity';
import { OrderItem } from 'src/orders/entities/order-item';
import { RecieptDetail } from 'src/reciepts/entities/reciept-detail';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  OneToMany,
  CreateDateColumn,
  UpdateDateColumn,
  DeleteDateColumn,
  ManyToOne,
} from 'typeorm';

@Entity()
export class Product {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    length: '32',
  })
  name: string;

  @Column({
    length: '32',
  })
  size: string;

  @Column({
    type: 'float',
  })
  price: number;

  @Column({
    length: '128',
    default: 'no_img_avaliable.jpg',
  })
  image: string;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @DeleteDateColumn()
  deletedAt: Date;

  @ManyToOne(() => Catagory, (catagory) => catagory.products)
  catagory: Catagory;

  @OneToMany(() => RecieptDetail, (recieptDetail) => recieptDetail.product)
  recieptDetails: RecieptDetail[];

  @Column()
  catagoryId: number;

  @OneToMany(() => OrderItem, (orderItem) => orderItem.product)
  orderItems: OrderItem[];
}
