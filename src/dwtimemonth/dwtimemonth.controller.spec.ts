import { Test, TestingModule } from '@nestjs/testing';
import { DwtimemonthController } from './dwtimemonth.controller';
import { DwtimemonthService } from './dwtimemonth.service';

describe('DwtimemonthController', () => {
  let controller: DwtimemonthController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [DwtimemonthController],
      providers: [DwtimemonthService],
    }).compile();

    controller = module.get<DwtimemonthController>(DwtimemonthController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
