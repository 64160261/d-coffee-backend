import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { DwtimemonthService } from './dwtimemonth.service';
import { CreateDwtimemonthDto } from './dto/create-dwtimemonth.dto';
import { UpdateDwtimemonthDto } from './dto/update-dwtimemonth.dto';

@Controller('dwtimemonth')
export class DwtimemonthController {
  constructor(private readonly dwtimemonthService: DwtimemonthService) {}

  @Post()
  create(@Body() createDwtimemonthDto: CreateDwtimemonthDto) {
    return this.dwtimemonthService.create(createDwtimemonthDto);
  }

  @Get()
  findAll() {
    return this.dwtimemonthService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.dwtimemonthService.findOne(+id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateDwtimemonthDto: UpdateDwtimemonthDto,
  ) {
    return this.dwtimemonthService.update(+id, updateDwtimemonthDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.dwtimemonthService.remove(+id);
  }
}
