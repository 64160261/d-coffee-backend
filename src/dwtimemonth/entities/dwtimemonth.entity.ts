import { Column, PrimaryGeneratedColumn } from 'typeorm';

export class Dwtimemonth {
  @PrimaryGeneratedColumn()
  TimeMonth_Id_: number;

  @Column()
  Time_Original: Date;

  @Column()
  Month: Date;

  @Column()
  TimeMonth_Quater: string;

  @Column()
  TimeMonth_Year: string;

  @Column()
  TimeMonth_Season: string;
}
