import { Module } from '@nestjs/common';
import { DwtimemonthService } from './dwtimemonth.service';
import { DwtimemonthController } from './dwtimemonth.controller';
import { Dwtimemonth } from './entities/dwtimemonth.entity';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [TypeOrmModule.forFeature([Dwtimemonth])],
  controllers: [DwtimemonthController],
  providers: [DwtimemonthService],
})
export class DwtimemonthModule {}
