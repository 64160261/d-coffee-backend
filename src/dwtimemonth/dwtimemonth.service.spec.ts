import { Test, TestingModule } from '@nestjs/testing';
import { DwtimemonthService } from './dwtimemonth.service';

describe('DwtimemonthService', () => {
  let service: DwtimemonthService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [DwtimemonthService],
    }).compile();

    service = module.get<DwtimemonthService>(DwtimemonthService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
