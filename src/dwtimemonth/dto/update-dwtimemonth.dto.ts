import { PartialType } from '@nestjs/mapped-types';
import { CreateDwtimemonthDto } from './create-dwtimemonth.dto';

export class UpdateDwtimemonthDto extends PartialType(CreateDwtimemonthDto) {}
