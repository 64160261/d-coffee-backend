import { IsNotEmpty } from 'class-validator';

export class CreateDwtimemonthDto {
  @IsNotEmpty()
  Time_Original: Date;

  @IsNotEmpty()
  Month: Date;

  @IsNotEmpty()
  TimeMonth_Quater: string;

  @IsNotEmpty()
  TimeMonth_Year: string;

  @IsNotEmpty()
  TimeMonth_Season: string;
}
