import { Injectable } from '@nestjs/common';
import { CreateDwtimemonthDto } from './dto/create-dwtimemonth.dto';
import { UpdateDwtimemonthDto } from './dto/update-dwtimemonth.dto';

@Injectable()
export class DwtimemonthService {
  create(createDwtimemonthDto: CreateDwtimemonthDto) {
    return 'This action adds a new dwtimemonth';
  }

  findAll() {
    return `This action returns all dwtimemonth`;
  }

  findOne(id: number) {
    return `This action returns a #${id} dwtimemonth`;
  }

  update(id: number, updateDwtimemonthDto: UpdateDwtimemonthDto) {
    return `This action updates a #${id} dwtimemonth`;
  }

  remove(id: number) {
    return `This action removes a #${id} dwtimemonth`;
  }
}
