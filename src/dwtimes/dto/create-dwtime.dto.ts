import { IsNotEmpty } from 'class-validator';

export class CreateDwtimeDto {
  @IsNotEmpty()
  Time_Original: Date;

  @IsNotEmpty()
  Time_Date: Date;

  @IsNotEmpty()
  Time_Month: Date;

  @IsNotEmpty()
  Time_Quater: string;

  @IsNotEmpty()
  Time_Year: string;

  @IsNotEmpty()
  Time_Day_of_week: string;

  @IsNotEmpty()
  Time_Season: string;
}
