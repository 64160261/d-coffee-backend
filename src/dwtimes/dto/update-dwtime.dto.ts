import { PartialType } from '@nestjs/mapped-types';
import { CreateDwtimeDto } from './create-dwtime.dto';

export class UpdateDwtimeDto extends PartialType(CreateDwtimeDto) {}
