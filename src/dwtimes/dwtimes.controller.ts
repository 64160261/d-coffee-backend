import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { DwtimesService } from './dwtimes.service';
import { CreateDwtimeDto } from './dto/create-dwtime.dto';
import { UpdateDwtimeDto } from './dto/update-dwtime.dto';

@Controller('dwtimes')
export class DwtimesController {
  constructor(private readonly dwtimesService: DwtimesService) {}

  @Post()
  create(@Body() createDwtimeDto: CreateDwtimeDto) {
    return this.dwtimesService.create(createDwtimeDto);
  }

  @Get()
  findAll() {
    return this.dwtimesService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.dwtimesService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateDwtimeDto: UpdateDwtimeDto) {
    return this.dwtimesService.update(+id, updateDwtimeDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.dwtimesService.remove(+id);
  }
}
