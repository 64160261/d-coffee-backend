import { Test, TestingModule } from '@nestjs/testing';
import { DwtimesController } from './dwtimes.controller';
import { DwtimesService } from './dwtimes.service';

describe('DwtimesController', () => {
  let controller: DwtimesController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [DwtimesController],
      providers: [DwtimesService],
    }).compile();

    controller = module.get<DwtimesController>(DwtimesController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
