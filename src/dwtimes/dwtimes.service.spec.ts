import { Test, TestingModule } from '@nestjs/testing';
import { DwtimesService } from './dwtimes.service';

describe('DwtimesService', () => {
  let service: DwtimesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [DwtimesService],
    }).compile();

    service = module.get<DwtimesService>(DwtimesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
