import { Module } from '@nestjs/common';
import { DwtimesService } from './dwtimes.service';
import { DwtimesController } from './dwtimes.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Dwtime } from './entities/dwtime.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Dwtime])],
  controllers: [DwtimesController],
  providers: [DwtimesService],
})
export class DwtimesModule {}
