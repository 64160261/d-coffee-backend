import { Injectable } from '@nestjs/common';
import { CreateDwtimeDto } from './dto/create-dwtime.dto';
import { UpdateDwtimeDto } from './dto/update-dwtime.dto';

@Injectable()
export class DwtimesService {
  create(createDwtimeDto: CreateDwtimeDto) {
    return 'This action adds a new dwtime';
  }

  findAll() {
    return `This action returns all dwtimes`;
  }

  findOne(id: number) {
    return `This action returns a #${id} dwtime`;
  }

  update(id: number, updateDwtimeDto: UpdateDwtimeDto) {
    return `This action updates a #${id} dwtime`;
  }

  remove(id: number) {
    return `This action removes a #${id} dwtime`;
  }
}
