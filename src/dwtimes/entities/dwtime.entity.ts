import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Dwtime {
  @PrimaryGeneratedColumn()
  Time_Id: number;

  @Column()
  Time_Original: Date;

  @Column()
  Time_Date: Date;

  @Column()
  Time_Month: Date;

  @Column()
  Time_Quater: string;

  @Column()
  Time_Year: string;

  @Column()
  Time_Day_of_week: string;

  @Column()
  Time_Season: string;
}
