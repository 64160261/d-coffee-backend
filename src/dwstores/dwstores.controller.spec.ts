import { Test, TestingModule } from '@nestjs/testing';
import { DwstoresController } from './dwstores.controller';
import { DwstoresService } from './dwstores.service';

describe('DwstoresController', () => {
  let controller: DwstoresController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [DwstoresController],
      providers: [DwstoresService],
    }).compile();

    controller = module.get<DwstoresController>(DwstoresController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
