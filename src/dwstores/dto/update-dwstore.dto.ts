import { PartialType } from '@nestjs/mapped-types';
import { CreateDwstoreDto } from './create-dwstore.dto';

export class UpdateDwstoreDto extends PartialType(CreateDwstoreDto) {}
