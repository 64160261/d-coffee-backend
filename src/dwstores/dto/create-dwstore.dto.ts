import { IsNotEmpty, Length } from 'class-validator';
export class CreateDwstoreDto {
  @IsNotEmpty()
  Store_Name: string;

  @IsNotEmpty()
  Store_Province: string;

  @IsNotEmpty()
  Store_District: string;

  @IsNotEmpty()
  Store_Subdistrict: string;

  @IsNotEmpty()
  @Length(5)
  Store_Zip: string;
}
