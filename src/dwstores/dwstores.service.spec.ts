import { Test, TestingModule } from '@nestjs/testing';
import { DwstoresService } from './dwstores.service';

describe('DwstoresService', () => {
  let service: DwstoresService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [DwstoresService],
    }).compile();

    service = module.get<DwstoresService>(DwstoresService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
