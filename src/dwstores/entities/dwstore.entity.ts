import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Dwstores {
  @PrimaryGeneratedColumn()
  Store_Id: number;

  @Column()
  Store_Name: string;

  @Column()
  Store_Province: string;

  @Column()
  Store_District: string;

  @Column()
  Store_Subdistrict: string;

  @Column()
  Store_Zip: string;
}
