import { Module } from '@nestjs/common';
import { DwstoresService } from './dwstores.service';
import { DwstoresController } from './dwstores.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Dwstores } from './entities/dwstore.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Dwstores])],
  controllers: [DwstoresController],
  providers: [DwstoresService],
})
export class DwstoresModule {}
