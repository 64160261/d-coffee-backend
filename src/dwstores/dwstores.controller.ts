import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { DwstoresService } from './dwstores.service';
import { CreateDwstoreDto } from './dto/create-dwstore.dto';
import { UpdateDwstoreDto } from './dto/update-dwstore.dto';

@Controller('dwstores')
export class DwstoresController {
  constructor(private readonly dwstoresService: DwstoresService) {}

  @Post()
  create(@Body() createDwstoreDto: CreateDwstoreDto) {
    return this.dwstoresService.create(createDwstoreDto);
  }

  @Get()
  findAll() {
    return this.dwstoresService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.dwstoresService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateDwstoreDto: UpdateDwstoreDto) {
    return this.dwstoresService.update(+id, updateDwstoreDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.dwstoresService.remove(+id);
  }
}
