import { Injectable } from '@nestjs/common';
import { CreateDwstoreDto } from './dto/create-dwstore.dto';
import { UpdateDwstoreDto } from './dto/update-dwstore.dto';

@Injectable()
export class DwstoresService {
  create(createDwstoreDto: CreateDwstoreDto) {
    return 'This action adds a new dwstore';
  }

  findAll() {
    return `This action returns all dwstores`;
  }

  findOne(id: number) {
    return `This action returns a #${id} dwstore`;
  }

  update(id: number, updateDwstoreDto: UpdateDwstoreDto) {
    return `This action updates a #${id} dwstore`;
  }

  remove(id: number) {
    return `This action removes a #${id} dwstore`;
  }
}
