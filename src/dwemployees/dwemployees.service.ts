import { Injectable } from '@nestjs/common';
import { CreateDwemployeeDto } from './dto/create-dwemployee.dto';
import { UpdateDwemployeeDto } from './dto/update-dwemployee.dto';

@Injectable()
export class DwemployeesService {
  create(createDwemployeeDto: CreateDwemployeeDto) {
    return 'This action adds a new dwemployee';
  }

  findAll() {
    return `This action returns all dwemployees`;
  }

  findOne(id: number) {
    return `This action returns a #${id} dwemployee`;
  }

  update(id: number, updateDwemployeeDto: UpdateDwemployeeDto) {
    return `This action updates a #${id} dwemployee`;
  }

  remove(id: number) {
    return `This action removes a #${id} dwemployee`;
  }
}
