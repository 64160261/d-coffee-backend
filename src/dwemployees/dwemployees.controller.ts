import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { DwemployeesService } from './dwemployees.service';
import { CreateDwemployeeDto } from './dto/create-dwemployee.dto';
import { UpdateDwemployeeDto } from './dto/update-dwemployee.dto';

@Controller('dwemployees')
export class DwemployeesController {
  constructor(private readonly dwemployeesService: DwemployeesService) {}

  @Post()
  create(@Body() createDwemployeeDto: CreateDwemployeeDto) {
    return this.dwemployeesService.create(createDwemployeeDto);
  }

  @Get()
  findAll() {
    return this.dwemployeesService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.dwemployeesService.findOne(+id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateDwemployeeDto: UpdateDwemployeeDto,
  ) {
    return this.dwemployeesService.update(+id, updateDwemployeeDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.dwemployeesService.remove(+id);
  }
}
