import { Module } from '@nestjs/common';
import { DwemployeesService } from './dwemployees.service';
import { DwemployeesController } from './dwemployees.controller';
import { Dwemployee } from './entities/dwemployee.entity';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [TypeOrmModule.forFeature([Dwemployee])],
  controllers: [DwemployeesController],
  providers: [DwemployeesService],
})
export class DwemployeesModule {}
