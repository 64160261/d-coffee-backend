import { IsNotEmpty } from 'class-validator';

export class CreateDwemployeeDto {
  @IsNotEmpty()
  name: string;

  @IsNotEmpty()
  position: string;
}
