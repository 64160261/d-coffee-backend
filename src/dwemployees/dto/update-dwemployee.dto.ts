import { PartialType } from '@nestjs/mapped-types';
import { CreateDwemployeeDto } from './create-dwemployee.dto';

export class UpdateDwemployeeDto extends PartialType(CreateDwemployeeDto) {}
