import { Test, TestingModule } from '@nestjs/testing';
import { DwemployeesController } from './dwemployees.controller';
import { DwemployeesService } from './dwemployees.service';

describe('DwemployeesController', () => {
  let controller: DwemployeesController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [DwemployeesController],
      providers: [DwemployeesService],
    }).compile();

    controller = module.get<DwemployeesController>(DwemployeesController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
