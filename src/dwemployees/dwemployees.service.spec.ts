import { Test, TestingModule } from '@nestjs/testing';
import { DwemployeesService } from './dwemployees.service';

describe('DwemployeesService', () => {
  let service: DwemployeesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [DwemployeesService],
    }).compile();

    service = module.get<DwemployeesService>(DwemployeesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
