import { Column, PrimaryGeneratedColumn } from 'typeorm';

export class Dwemployee {
  @PrimaryGeneratedColumn()
  Employee_Id: number;

  @Column()
  Employee_Name: string;

  @Column()
  Employee_Position: string;
}
