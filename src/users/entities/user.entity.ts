import { Employee } from 'src/employees/entities/employee.entity';
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  JoinColumn,
  OneToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    length: '32',
    unique: true,
  })
  login: string;

  @Column({
    length: '32',
  })
  name: string;

  @Column()
  password: string;

  @Column({
    length: '128',
    default: 'no_img_avaliable.jpg',
  })
  image: string;

  @Column({
    length: '32',
  })
  role: string;

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @DeleteDateColumn()
  deletedAt: Date;

  @OneToOne(() => Employee, (employees) => employees.user)
  @JoinColumn()
  employee: Employee;
}
