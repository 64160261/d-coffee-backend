import { BillDetail } from 'src/bills/entities/bill-detail';
import { CheckmaterialDetail } from 'src/checkmaterials/entities/checkmaterial-detail';
import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from 'typeorm';

@Entity()
export class Material {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  minQuantity: number;

  @Column()
  quantity: number;

  @Column()
  unit: number;

  @Column()
  price: number;

  @Column({
    length: '128',
    default: 'no_img_avaliable.jpg',
  })
  image: string;

  @OneToMany(
    () => CheckmaterialDetail,
    (checkmaterialDetail) => checkmaterialDetail.material,
  )
  checkmaterialDetails: CheckmaterialDetail[];

  @OneToMany(() => BillDetail, (billDetail) => billDetail.material)
  billDetails: BillDetail;
}
