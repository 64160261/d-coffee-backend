import { IsNotEmpty, Length, IsNumber, Min } from 'class-validator';

export class CreateMaterialDto {
  @IsNotEmpty()
  name: string;

  @IsNotEmpty()
  unit: number;

  @IsNotEmpty()
  price: number;

  @IsNotEmpty()
  quantity: number;

  @IsNotEmpty()
  minQuantity: number;

  image = 'no_img_avaliable.jpg';
}
